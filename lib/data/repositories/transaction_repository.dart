import 'package:learnriverpod/domain/entities/result.dart';
import 'package:learnriverpod/domain/entities/transaction.dart';

abstract interface class TransactionRepository {
  Future<Result<List<Transaction>>> getUserTransactions({required String uid});
  Future<Result<Transaction>> createTransaction({required Transaction transaction});
  
}