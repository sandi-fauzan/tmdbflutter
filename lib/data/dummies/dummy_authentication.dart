import 'package:learnriverpod/data/repositories/authentication.dart';
import 'package:learnriverpod/domain/entities/result.dart';

class DummyAuthentication implements Authentication {
  @override
  Future<Result<String>> login({String? email, String? password}) async {
    await Future.delayed(Duration(seconds: 2));
    return Result.success('123');
  }

  @override
  Future<Result<void>> logout() async {
    await Future.delayed(Duration(seconds: 2));
    return Result.success(null);
  }

  @override
  Future<Result<String>> register({String? email, String? password}) async {
    await Future.delayed(Duration(seconds: 2));
    return Result.success('123');
  }

  @override
  String? getLoggedInUserId() {
    return '123';
  }
}