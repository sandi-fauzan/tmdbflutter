import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:learnriverpod/data/repositories/movie_repository.dart';
import 'package:learnriverpod/domain/entities/actor.dart';
import 'package:learnriverpod/domain/entities/movie.dart';
import 'package:learnriverpod/domain/entities/movie_detail.dart';
import 'package:learnriverpod/domain/entities/result.dart';

class TmdbMovieRepository implements MovieRepository {
  final Dio? _dio;
  final String _accessToken = "eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI4NWE4OTllZWRlZTU3MTE3MzFmZmNmZDEzYmE5Mzg3NyIsInN1YiI6IjY2NWVjOTI4ZTFhYTgxNTM3OTFjMzExOCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.b9kt4v9NRXFBhR1GrC6O3wRmw-YT2BNTom0oUdGOd1U";

  late final Options _options = Options(
    headers: {
      "Authorization" : "Bearer $_accessToken",
      "accept"        : "application/json"
    }
  );

  TmdbMovieRepository({Dio? dio}) : _dio = dio ?? Dio();

  @override
  Future<Result<List<Actor>>> getActors({required int movieId, required int id}) async {
    try {
      final response = await _dio!.get(
        'https://api.themoviedb.org/3/movie/$movieId/credits',
        options: _options
      );

      final results = List<Map<String, dynamic>>.from(response.data['cast']);
      return Result.success(results.map((e) => Actor.fromJSON(e)).toList());
    } catch (e) {
      return Result.failed(e.toString());
    }
  }

  @override
  Future<Result<MovieDetail>> getDetail({required int id}) async {
    try {
      final response = await _dio!.get(
        'https://api.themoviedb.org/3/movie/$id',
        options: _options
      );

      return Result.success(MovieDetail.fromJSON(response.data));
    } catch (e) {
      return Result.failed(e.toString());
    }
  }

  @override
  Future<Result<List<Movie>>> getNowPlaying({int page = 1}) {
    return _getMovies(_MovieCategory.nowPlaying.toString(), page: page);
  }

  @override
  Future<Result<List<Movie>>> getUpcoming({int page = 1}) {
    return _getMovies(_MovieCategory.upcoming.toString(), page: page);
  }

  Future<Result<List<Movie>>> _getMovies(String category, { int page = 1 }) async {
    try {
      final response = await _dio!.get(
        'https://api.themoviedb.org/3/movie/$category?page=$page',
        options: _options
      );

      final results = List<Map<String, dynamic>>.from(response.data['results']);
      return Result.success(results.map((e) => Movie.fromJSON(e)).toList());
    } catch (e) {
      return Result.failed(e.toString());
    }
  }

  
  
}


enum _MovieCategory {
  nowPlaying('now_playing'),
  upcoming('upcoming');

  final String _instring;
  const _MovieCategory(String instring) : _instring = instring;

  @override
  String toString() => _instring;
}