import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:learnriverpod/domain/entities/result.dart';
import 'package:learnriverpod/domain/entities/user.dart';
import 'package:learnriverpod/domain/usecases/get_logged_in_user/get_logged_in_user.dart';
import 'package:learnriverpod/domain/usecases/login/login.dart';
import 'package:learnriverpod/domain/usecases/register/register.dart';
import 'package:learnriverpod/domain/usecases/register/register_params.dart';
import 'package:learnriverpod/domain/usecases/top_up/top_up.dart';
import 'package:learnriverpod/domain/usecases/top_up/top_up_param.dart';
import 'package:learnriverpod/domain/usecases/upload_profile_picture/upload_profile_picture.dart';
import 'package:learnriverpod/domain/usecases/upload_profile_picture/upload_profile_picture_param.dart';
import 'package:learnriverpod/presentation/providers/movie/now_playing_provider.dart';
import 'package:learnriverpod/presentation/providers/movie/upcoming_provider.dart';
import 'package:learnriverpod/presentation/providers/transaction_data/transaction_data_provider.dart';
import 'package:learnriverpod/presentation/providers/usecases/get_logged_in_user_provider.dart';
import 'package:learnriverpod/presentation/providers/usecases/login_provider.dart';
import 'package:learnriverpod/presentation/providers/usecases/logout_provider.dart';
import 'package:learnriverpod/presentation/providers/usecases/register_provider.dart';
import 'package:learnriverpod/presentation/providers/usecases/top_up_provider.dart';
import 'package:learnriverpod/presentation/providers/usecases/upload_profile_picture_provider.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'user_data_provider.g.dart';

@Riverpod(keepAlive: true)
class UserData extends _$UserData {
  @override 
  Future<User?> build() async {
    GetLoggedInUser getLoggedInUser = ref.read(getLoggedInUserProvider);
    var userResult = await getLoggedInUser(null);

    switch(userResult){
      case Success(value: final user):
        _getMovies();
        return user;
      case Failed(message: final _):
        return null;
    }
  }

  Future<void> login({required String email, required String password}) async {
    state = AsyncLoading();

    Login login = ref.read(loginProvider);
    var result = await login(LoginParams(email: email, password: password));

    switch(result){
      case Success(value: final user):
        _getMovies();
        state = AsyncData(user);
      case Failed(: final message):
        state = AsyncError(FlutterError(message), StackTrace.current);
        state = AsyncData(null);
    }
  }

  Future<void> register({required String email, required String password, required String name, String? imageUrl}) async {
    state = AsyncLoading();

    Register register = ref.read(registerProvider);
    var result = await register(RegisterParams(email: email, password: password, name: name));

    switch(result){
      case Success(value: final user):
        _getMovies();
        state = AsyncData(user);
      case Failed(: final message):
        state = AsyncError(FlutterError(message), StackTrace.current);
        state = AsyncData(null);
    }
  }

  Future<void> refreshUserData() async {
    GetLoggedInUser getLoggedInUser = ref.read(getLoggedInUserProvider);
    var userResult = await getLoggedInUser(null);

    if(userResult case Success(value: final user)){
      state = AsyncData(user);
    }
  } 

  Future<void> logout() async {
    var logout = ref.read(logoutProvider);
    var result = await logout(null);

    switch(result){
      case Success(value: _):
        state = const AsyncData(null);
      case Failed(: final message):
        state = AsyncError(FlutterError(message), StackTrace.current);
        state = AsyncData(state.valueOrNull);
    }
  }

  Future<void> topUp(int amount) async {
    TopUp topUp = ref.read(topUpProvider);
    String? userId = state.valueOrNull?.uid;

    if(userId != null){
      var result = await topUp(TopUpParam(userId: userId, amount: amount));

      if(result.isSuccess){
        refreshUserData();
        ref.read(transactionDataProvider.notifier).refreshTransactionData();
      }
    }
  }

  Future<void> uploadProfilePicture({required User user, required File imageFile}) async {
    UploadProfilePicture uploadProfilePicture = ref.read(uploadProfilePictureProvider);

    var result = await uploadProfilePicture(UploadProfilePictureParam(user: user, imageFile: imageFile));

    if(result case Success(value: final user)) {
      state = AsyncData(user);
    }
  }

  void _getMovies(){
    ref.read(nowPLayingProvider.notifier).getMovies();
    ref.read(upcomingProvider.notifier).getMovies();
  }
}