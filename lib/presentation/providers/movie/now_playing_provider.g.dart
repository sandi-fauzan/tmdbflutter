// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'now_playing_provider.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$nowPLayingHash() => r'078f0f2c6ded9fd6edc30eb036109c926ee42e43';

/// See also [NowPLaying].
@ProviderFor(NowPLaying)
final nowPLayingProvider =
    AsyncNotifierProvider<NowPLaying, List<Movie>>.internal(
  NowPLaying.new,
  name: r'nowPLayingProvider',
  debugGetCreateSourceHash:
      const bool.fromEnvironment('dart.vm.product') ? null : _$nowPLayingHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$NowPLaying = AsyncNotifier<List<Movie>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
