import 'package:learnriverpod/data/firebase/firebase_transaction_repository.dart';
import 'package:learnriverpod/data/repositories/transaction_repository.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'transaction_repository_provider.g.dart';

@riverpod
TransactionRepository transactionRepository(TransactionRepositoryRef ref) => FirebaseTransactionRepository();