import 'package:flutter/material.dart';

Map<double, SizedBox> _verticalSpaces = {};
Map<double, SizedBox> _horizontalSpaces = {};

SizedBox verticalSpace(double height) {
  if (_verticalSpaces.containsKey(height)) {
    return _verticalSpaces[height]!;
  }
  return _verticalSpaces[height] = SizedBox(height: height);
}

SizedBox horizontalSpace(double width) {
  if (_horizontalSpaces.containsKey(width)) {
    return _horizontalSpaces[width]!;
  }
  return _horizontalSpaces[width] = SizedBox(width: width);
}