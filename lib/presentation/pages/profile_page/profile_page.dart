import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/pages/profile_page/methods/profile_item.dart';
import 'package:learnriverpod/presentation/pages/profile_page/methods/user_info.dart';
import 'package:learnriverpod/presentation/providers/user_data/user_data_provider.dart';

class ProfilePage extends ConsumerWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ListView(
      children: [
        Padding(padding: const EdgeInsets.all(24), child: Column(
          children: [
            verticalSpace(20),
            ...userInfo(ref),
            verticalSpace(20),
            const Divider(),
            verticalSpace(20),
            profileItem('Update Profile'),
            profileItem('My Wallet'),
            profileItem('Change Password'),
            profileItem('Change Language'),
            const Divider(),
            profileItem('Contact Us'),
            profileItem('Privacy Policy'),
            profileItem('Terms and Conditions'),
            verticalSpace(60),
            SizedBox(
              width: double.infinity,
              child: ElevatedButton(onPressed: () {
                ref.read(userDataProvider.notifier).logout();
              }, child: const Text('Logout'))),
            verticalSpace(20),
            const Text('Version 1.0.0', style: TextStyle(fontSize: 11),),
            verticalSpace(80)
          ],
        ),)
      ],
    );
  }
}