import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/presentation/extensions/build_context_extension.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/providers/router/router_provider.dart';
import 'package:learnriverpod/presentation/providers/user_data/user_data_provider.dart';
import 'package:learnriverpod/presentation/widgets/flix_text_field.dart';

class LoginPage extends ConsumerWidget {
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  LoginPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    ref.listen(
      userDataProvider, (previous, next){
        if(next is AsyncData) {
          if(next.value != null){
            ref.read(routerProvider).goNamed('main');
          }
        } else if(next is AsyncError){
          context.showSnackbar(next.error.toString());
        }
      });
    return Scaffold(
      body: Center(
        child: ListView(
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 200),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  FlixTextField(labelText: 'Email', controller: emailController),
                  verticalSpace(30),
                  FlixTextField(labelText: 'Password', controller: passwordController, obscureText: true,),
                  Align(
                    alignment: Alignment.centerRight,
                    child: TextButton(
                      onPressed: () {},
                      child: const Text(
                        'Forgot Password?',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  verticalSpace(30),
                  switch(ref.watch(userDataProvider)){
                    AsyncData(:final value) => value == null ?
                      SizedBox(
                        width: double.infinity,
                        child: ElevatedButton(
                          onPressed: () {
                            ref.read(userDataProvider.notifier).login(email: emailController.text, password: passwordController.text);
                          },
                          child: const Text('Login'),
                        ),
                      ) :
                      const Center(
                        child: CircularProgressIndicator(),
                      ),
                      _ => const Center(
                        child: CircularProgressIndicator()
                      )
                  },
                  verticalSpace(30),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text("Don't have an account?"),
                      TextButton(
                        onPressed: () {
                          ref.read(routerProvider).goNamed('register');
                        },
                        child: const Text('Register'),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}