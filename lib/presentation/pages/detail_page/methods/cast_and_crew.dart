import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/domain/entities/movie.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/providers/movie/actors_provider.dart';
import 'package:learnriverpod/presentation/widgets/network_image_card.dart';

List<Widget> castAndCrew({
  required Movie movie,
  required WidgetRef ref,
}) => [
  const Padding(
    padding: EdgeInsets.only(left: 24, bottom: 20),
    child: Text('Cast & Crew', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
  ),
  SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        horizontalSpace(24),
        ...(ref.watch(actorsProvider(movieId: movie.id)).whenOrNull(
          data: (actors) => actors.where((element) => element.profilePath != null)
                .map((e) => Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Column(
                    children: [
                      NetworkImageCard(imageUrl: 'https://image.tmdb.org/t/p/w185/${e.profilePath}', width: 100, height: 152, borderRadius: 10, fit: BoxFit.cover,),
                      SizedBox(
                        width: 100,
                        child: Text(e.name, maxLines: 2, textAlign: TextAlign.center, overflow: TextOverflow.ellipsis, style: const TextStyle(fontSize: 12),),
                      ),
                    ],
                  ),
                ))
        ) ?? []),
        horizontalSpace(20)
      ],
    ),
  )
];