import 'package:flutter/material.dart';
import 'package:learnriverpod/domain/entities/movie.dart';

List<Widget> background(Movie movie) => [
  Image.network('https://image.tmdb.org/t/p/w500/${movie.posterPath}', fit: BoxFit.cover, height: double.infinity, width: double.infinity),
  Container(
    decoration: BoxDecoration(
      gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomCenter,
        colors: [
          Colors.black.withOpacity(0.5),
          Colors.black.withOpacity(0.6),
          Colors.black.withOpacity(0.9),
          Colors.black
        ],
        stops: [0.1, 0.3, 0.7, 1]
      )
    ),
  )
];