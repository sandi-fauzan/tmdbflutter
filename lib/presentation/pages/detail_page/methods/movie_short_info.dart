import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/domain/entities/movie_detail.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';

List<Widget> movieShortInfo({
  required AsyncValue<MovieDetail?> asyncMovieDetail,
  required BuildContext context,
}) => [
  Row(
    children: [
      SizedBox(
        width: 14,
        height: 14,
        child: Image.asset('assets/duration.png'),
      ),
      horizontalSpace(5),
      SizedBox(
        width: 95,
        child: Text(
            asyncMovieDetail.when(data: (movieDetail) => movieDetail != null ? movieDetail.runtime.toString() + ' minutes' : '-',
              error: (error, stackTrace) => '-', 
              loading: () => 'Loading...'),
          style: const TextStyle(fontSize: 12),
        ),
      ),
      SizedBox(
        width: 14,
        height: 14,
        child: Image.asset('assets/genre.png'),
      ),
      horizontalSpace(5),
      SizedBox(
        width: MediaQuery.of(context).size.width - 48 - 95 - 14 - 14 - 5 - 5,
        child: asyncMovieDetail.when(
          data: (movieDetail){
            String genres = movieDetail?.genres.join(', ') ?? '-';
            return Text(genres, maxLines: 1, overflow: TextOverflow.ellipsis, style: const TextStyle(fontSize: 12),);
          }, 
          error: (error, stackTrace) => const Text('-', style: TextStyle(fontSize: 12)), 
          loading: () => const Text('-', style: TextStyle(fontSize: 12)),
        ),
      ),
    ],
  ),
  verticalSpace(10),
  Row(
    mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      SizedBox(
        width: 14,
        height: 14,
        child: Image.asset('assets/star.png'),
      ),
      horizontalSpace(5),
      SizedBox(
        width: 95,
        child: Text(
          asyncMovieDetail.when(data: (movieDetail) => movieDetail != null ? movieDetail.voteAverage.toString() : '-',
            error: (error, stackTrace) => '-', 
            loading: () => 'Loading...'),
          style: const TextStyle(fontSize: 12),
        ),
      ),
    ],
  ),
];