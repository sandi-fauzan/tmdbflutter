import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/domain/entities/movie_detail.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';

List<Widget> movieOverview(AsyncValue<MovieDetail?> asyncMovieDetail) => [
  const Text('Overview', style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold)),
  verticalSpace(10),
  asyncMovieDetail.when(
    data: (movieDetail) => Text(movieDetail != null ? movieDetail.overview : '-', style: TextStyle(fontSize: 12)), 
    error: (error, stackTrace) => const Text('Error'), 
    loading: () => const Text('-'))
];