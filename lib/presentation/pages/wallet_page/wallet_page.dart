import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/providers/router/router_provider.dart';
import 'package:learnriverpod/presentation/widgets/back_navigation_bar.dart';

class WalletPage extends ConsumerWidget {
  const WalletPage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.all(24),
            child: Column(
              children: [
                BackNavigationBar(title: 'My Wallet', onTap: () => ref.read(routerProvider).pop(),),
                verticalSpace(24),
                // wallet card
                
              ],
            ),
          )
        ],
      )
    );
  }
}