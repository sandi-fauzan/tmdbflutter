import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/presentation/extensions/build_context_extension.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/providers/router/router_provider.dart';
import 'package:learnriverpod/presentation/providers/user_data/user_data_provider.dart';
import 'package:learnriverpod/presentation/widgets/flix_text_field.dart';

class RegisterPage extends ConsumerStatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  ConsumerState<RegisterPage> createState() => _RegisterPageState();
}

class _RegisterPageState extends ConsumerState<RegisterPage> {
  final TextEditingController nameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController retypePasswordController = TextEditingController();
  final TextEditingController emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    ref.listen(userDataProvider, (previous, next) {
      if(next is AsyncData && next.value != null) {
        ref.read(routerProvider).goNamed('main');
      } else if(next is AsyncError) {
        context.showSnackbar(next.error.toString());
      }
    });
    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 200),
            child: Column(
              children: [
                FlixTextField(labelText: 'Email', controller: emailController),
                verticalSpace(24),
                FlixTextField(labelText: 'Name', controller: nameController),
                verticalSpace(24),
                FlixTextField(labelText: 'Password', controller: passwordController, obscureText: true,),
                verticalSpace(24),
                FlixTextField(labelText: 'Retype Password', controller: retypePasswordController, obscureText: true,),
                verticalSpace(24),
                switch(ref.watch(userDataProvider)) {
                  AsyncData(:final value) => value == null ?
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          if(passwordController.text != retypePasswordController.text){
                            ref.read(userDataProvider.notifier).register(email: emailController.text, password: passwordController.text, name: nameController.text);
                          } else {
                            context.showSnackbar('Retype password did not match');
                          }
                        },
                        child: const Text('Register'),
                      ),
                    ) : Center(child: CircularProgressIndicator(),),
                    _ => Center(child: CircularProgressIndicator(),)
                  
                },
                verticalSpace(30),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    const Text('Already have an account?'),
                    TextButton(
                      onPressed: () {
                        ref.read(routerProvider).goNamed('login');
                      },
                      child: const Text('Login'),
                    )
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}