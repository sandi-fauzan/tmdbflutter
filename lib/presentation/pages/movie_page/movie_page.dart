import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/pages/movie_page/methods/movie_list.dart';
import 'package:learnriverpod/presentation/pages/movie_page/methods/promotion_list.dart';
import 'package:learnriverpod/presentation/providers/movie/now_playing_provider.dart';
import 'package:learnriverpod/presentation/providers/movie/upcoming_provider.dart';
import 'package:learnriverpod/presentation/providers/router/router_provider.dart';
import './methods/user_info.dart';
import './methods/search_bar.dart';

class MoviePage extends ConsumerWidget {
  final List<String> promotionImageFileNames = const [
    'popcorn.jpg',
    'buy1get1.jpg',
  ]; 
  const MoviePage({super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return ListView(
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            userInfo(ref),
            verticalSpace(20),
            searchBar(context),
            verticalSpace(20),
            ...movieList(title: 'Now Playing', movies: ref.watch(nowPLayingProvider), onTap: (movie) {
              ref.read(routerProvider).pushNamed('detail', extra: movie);
            }),
            verticalSpace(20),
            ...promotionList(promotionImageFileNames),
            verticalSpace(20),
            ...movieList(title: 'Upcoming', movies: ref.watch(upcomingProvider), onTap: (movie) {
            }),
            verticalSpace(100)
          ],
        )
      ],
    );
  }
}