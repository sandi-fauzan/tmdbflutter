import 'package:flutter/material.dart';

List<Widget> promotionList(List<String> promotionImageFileNames) => [
  Padding(padding: EdgeInsets.only(left: 24, bottom: 15), child: Text('Promotions', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold))),
  SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Row(
      children: promotionImageFileNames.map((fileName) => Padding(
        padding: EdgeInsets.only(left: fileName == promotionImageFileNames.first ? 24 : 10, right: fileName == promotionImageFileNames.last ? 24 : 0),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Image.asset('assets/$fileName', width: 200, height: 130, fit: BoxFit.cover),
        ),
      )).toList(),
    ),
  )
];