import 'package:flutter/material.dart';

Widget searchBar(BuildContext context) => Row(
  children: [
    Container(
      width: MediaQuery.of(context).size.width - 24 - 24 - 90,
      height: 50,
      margin: const EdgeInsets.only(right: 10, left: 24),
      padding: const EdgeInsets.symmetric(horizontal: 10),
      decoration: BoxDecoration(
        color: Color.fromARGB(255, 35, 34, 28),
        borderRadius: BorderRadius.circular(10),
      ),
      child: TextField(
        style: TextStyle(color: Colors.grey.shade400),
        decoration: InputDecoration(
          hintText: 'Search movie...',
          hintStyle: TextStyle(color: Colors.grey.shade400),
          border: InputBorder.none,
          icon: Icon(Icons.movie, color: Colors.grey.shade400),
        ),
      )
    ),
    SizedBox(
      width: 80,
      height: 50,
      child: ElevatedButton(
        onPressed: () {},
        style: ElevatedButton.styleFrom(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
        child: Center(
          child: Icon(Icons.search)
        ),
      ),
    )
  ],
);