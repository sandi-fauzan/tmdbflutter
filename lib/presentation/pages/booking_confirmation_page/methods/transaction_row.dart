import 'package:flutter/material.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';

Widget transactionRow({
  required String title,
  required String value,
  required double width,
}) =>
    Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(
            width: 110,
            child: Text(
              title,
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          horizontalSpace(20),
          SizedBox(
            width: width - 110 - 20 - 50,
            child: Text(
              value,
              style: TextStyle(
                fontSize: 10,
              ),
            )
          )
        ],
      ),
    );