import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:learnriverpod/domain/entities/movie_detail.dart';
import 'package:learnriverpod/domain/entities/result.dart';
import 'package:learnriverpod/domain/entities/transaction.dart';
import 'package:learnriverpod/domain/usecases/create_transaction/create_transaction.dart';
import 'package:learnriverpod/domain/usecases/create_transaction/create_transaction_param.dart';
import 'package:learnriverpod/presentation/extensions/build_context_extension.dart';
import 'package:learnriverpod/presentation/extensions/int_extension.dart';
import 'package:learnriverpod/presentation/misc/constants.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/pages/booking_confirmation_page/methods/transaction_row.dart';
import 'package:learnriverpod/presentation/providers/router/router_provider.dart';
import 'package:learnriverpod/presentation/providers/transaction_data/transaction_data_provider.dart';
import 'package:learnriverpod/presentation/providers/usecases/create_transaction_provider.dart';
import 'package:learnriverpod/presentation/providers/user_data/user_data_provider.dart';
import 'package:learnriverpod/presentation/widgets/back_navigation_bar.dart';
import 'package:learnriverpod/presentation/widgets/network_image_card.dart';

class BookingConfirmationPage extends ConsumerWidget {
  final (MovieDetail, Transaction) transactionDetail;
  const BookingConfirmationPage(this.transactionDetail, {super.key});

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    var (movieDetail, transaction) = transactionDetail;

    transaction = transaction.copyWith(
      total: transaction.ticketAmount! * transaction.ticketPrice! + transaction.adminFee!,
    );

    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: const EdgeInsets.fromLTRB(24, 24, 24, 48),
            child: Column(
              children: [
                BackNavigationBar(title: 'Booking Confirmation', onTap: () => ref.read(routerProvider).pop()),
                verticalSpace(20),
                NetworkImageCard(imageUrl: 'https://image.tmdb.org/t/p/w500/${movieDetail.posterPath}', width: MediaQuery.of(context).size.width - 48, height: (MediaQuery.of(context).size.width - 48) * 0.5),
                verticalSpace(20),
                SizedBox(
                  width: MediaQuery.of(context).size.width - 48,
                  child: Text(
                    transaction.title,
                    style: const TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Divider(color: ghostWhite),
                transactionRow(title: 'Showing Date', value: DateFormat('EEEE, d MMMM y').format(DateTime.fromMillisecondsSinceEpoch(transaction.watchingTime ?? 0)), width: MediaQuery.of(context).size.width - 48),
                transactionRow(title: 'Theater', value: '${transaction.theaterName}', width: MediaQuery.of(context).size.width - 48),
                transactionRow(title: 'Seat Numbers', value: transaction.seats!.join(', '), width: MediaQuery.of(context).size.width - 48),
                transactionRow(title: '# of Ticket', value: '${transaction.ticketAmount} ticket(s)', width: MediaQuery.of(context).size.width - 48),
                transactionRow(title: 'Ticket price', value: '${transaction.ticketPrice?.toIDRCurrencyFormat()} ticket(s)', width: MediaQuery.of(context).size.width - 48),
                transactionRow(title: 'Adm. Fee', value: '${transaction.adminFee.toIDRCurrencyFormat()} ticket(s)', width: MediaQuery.of(context).size.width - 48),
                const Divider(color: ghostWhite),
                transactionRow(title: 'Total', value: '${transaction.total.toIDRCurrencyFormat()} ticket(s)', width: MediaQuery.of(context).size.width - 48),

                verticalSpace(40),
                ElevatedButton(
                  onPressed: () async {
                    int transactionTime = DateTime.now().millisecondsSinceEpoch;

                    transaction = transaction.copyWith(
                      transactionTime: transactionTime,
                      id: 'flx-$transactionTime-${transaction.uid}',
                    );

                    CreateTransaction createTransaction = ref.read(createTransactionProvider);

                    await createTransaction(CreateTransactionParam(transaction: transaction)).then(
                      (result) {
                        switch(result) {
                          case Success(value: _) : ref.read(transactionDataProvider.notifier).refreshTransactionData();
                            ref.read(userDataProvider.notifier).refreshUserData();
                            ref.read(routerProvider).goNamed('main');

                          case Failed(:final message):
                            context.showSnackbar(message);
                        }
                      }
                    );
                  }, 
                  style: ElevatedButton.styleFrom(
                    backgroundColor: saffron,
                    foregroundColor: backgroundColor,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 12),
                  ),
                  child: const Text('Pay Now'))
              ],
            ),
          )
        ],
      ),
    );
  }
}