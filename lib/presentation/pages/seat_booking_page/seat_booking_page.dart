import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:learnriverpod/domain/entities/movie_detail.dart';
import 'package:learnriverpod/domain/entities/transaction.dart';
import 'package:learnriverpod/presentation/extensions/build_context_extension.dart';
import 'package:learnriverpod/presentation/misc/constants.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/pages/seat_booking_page/methods/legend.dart';
import 'package:learnriverpod/presentation/pages/seat_booking_page/methods/movie_screen.dart';
import 'package:learnriverpod/presentation/pages/seat_booking_page/methods/seat_section.dart';
import 'package:learnriverpod/presentation/providers/router/router_provider.dart';
import 'package:learnriverpod/presentation/widgets/back_navigation_bar.dart';
import 'package:learnriverpod/presentation/widgets/seat.dart';

class SeatBookingPage extends ConsumerStatefulWidget {
  final (MovieDetail, Transaction) transactionDetail;

  const SeatBookingPage({super.key, required this.transactionDetail});

  @override
  ConsumerState<SeatBookingPage> createState() => _SeatBookingPageState();
}

class _SeatBookingPageState extends ConsumerState<SeatBookingPage> {
  List<int> selectedSeats = [];
  List<int> reservedSeats = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Random random = Random();

    int reservedNumber = random.nextInt(36) + 1;

    while(reservedSeats.length < 8){
      if(!reservedSeats.contains(reservedNumber)){
        reservedSeats.add(reservedNumber);
      }

      reservedNumber = random.nextInt(36) + 1;
    }
    
  }

  @override
  Widget build(BuildContext context) {
    final (movieDetail, transaction) = widget.transactionDetail;

    return Scaffold(
      body: ListView(
        children: [
          Padding(
            padding: EdgeInsets.all(24),
            child: Column(
              children: [
                BackNavigationBar(title: movieDetail.title, onTap: () => ref.read(routerProvider).pop()),
                movieScreen(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    seatSection(
                      seatNumbers: List.generate(18, (index) => index + 1),
                      onTap: onSeatTap,
                      seatStatusChecker: seatStatusChecker,
                    ),
                    horizontalSpace(30),
                    seatSection(
                      seatNumbers: List.generate(18, (index) => index + 19),
                      onTap: onSeatTap,
                      seatStatusChecker: seatStatusChecker,
                    ),
                  ],
                ),
                verticalSpace(20),
                legend(),
                verticalSpace(40),
                Text(
                  '${selectedSeats.length} seats selected',
                ),
                Text(
                  'Selected Seats: ${selectedSeats.join(', ')}',
                  style: TextStyle(
                    fontSize: 10,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                verticalSpace(40),
                SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: saffron,
                      foregroundColor: backgroundColor,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    onPressed: () {
                      if(selectedSeats.isEmpty){
                        context.showSnackbar('Please select at least one seat');
                      } else {
                        var updatedTransaction = transaction.copyWith(
                          seats: (selectedSeats..sort()).map((e) => '$e').toList(),
                          ticketAmount: selectedSeats.length,
                          ticketPrice: 25000
                        );
                        ref.read(routerProvider).pushNamed('booking-confirmation', extra: (movieDetail, updatedTransaction));
                      }
                    },
                    child: Text('Continue to Payment'),
                  ),
                )
                // number of selected
                // button
              ],
            )
          )
        ],
      )
    );
  }

  void onSeatTap(int seatNumber) {
    if (!selectedSeats.contains(seatNumber)) {
      setState(() {
        selectedSeats.add(seatNumber);
      });
    } else {
      setState(() {
        selectedSeats.remove(seatNumber);
      });
    }
  }

  SeatStatus seatStatusChecker(int seatNumber) {
    if (reservedSeats.contains(seatNumber)) {
      return SeatStatus.reserved;
    } else if (selectedSeats.contains(seatNumber)) {
      return SeatStatus.selected;
    } else {
      return SeatStatus.available;
    }
  }
}