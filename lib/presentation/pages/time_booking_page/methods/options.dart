import 'package:flutter/material.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/widgets/selectable_card.dart';

List<Widget> options<T>(
  {
    required String title,
    required List<T> options,
    required T? selectedItem,
    String Function(T object)? converter,
    bool Function(T object)? isOptionEnable,
    required void Function(T object) onTap,

  }
) => [
  Padding(
    padding: EdgeInsets.only(left: 24),
    child: Text(
      title,
      style: TextStyle(
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    ),
  ),
  verticalSpace(10),
  SingleChildScrollView(
    scrollDirection: Axis.horizontal,
    child: Row(
      children: options.map((e) => Padding(
        padding: EdgeInsets.only(left: e == options.first ? 24 : 10, right: e == options.last ? 24 : 0),
        child: SelectableCard(
            text: converter != null ? converter(e) : e.toString(), 
            onTap: () => onTap(e),
            isSelected: e == selectedItem,
            isEnable: isOptionEnable != null ? isOptionEnable(e) : true,
          
          ),
      )).toList(),

    ),
  ),
];