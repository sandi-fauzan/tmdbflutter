import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:intl/intl.dart';
import 'package:learnriverpod/domain/entities/movie_detail.dart';
import 'package:learnriverpod/domain/entities/transaction.dart';
import 'package:learnriverpod/presentation/extensions/build_context_extension.dart';
import 'package:learnriverpod/presentation/misc/constants.dart';
import 'package:learnriverpod/presentation/misc/methods.dart';
import 'package:learnriverpod/presentation/pages/time_booking_page/methods/options.dart';
import 'package:learnriverpod/presentation/providers/router/router_provider.dart';
import 'package:learnriverpod/presentation/providers/user_data/user_data_provider.dart';
import 'package:learnriverpod/presentation/widgets/back_navigation_bar.dart';
import 'package:learnriverpod/presentation/widgets/network_image_card.dart';
import 'package:learnriverpod/presentation/widgets/selectable_card.dart';

class TimeBookingPage extends ConsumerStatefulWidget {
  final MovieDetail movieDetail;

  const TimeBookingPage(this.movieDetail, {super.key});

  @override
  ConsumerState<TimeBookingPage> createState() => _TimeBookingPageState();
}

class _TimeBookingPageState extends ConsumerState<TimeBookingPage> {
  final List<String> theaters = [
    'XXI Plaza Indonesia',
    'CGV Grand Indonesia',
    'Cinemaxx Plaza Indonesia',
    'Cinemaxx Grand Indonesia',
    'XXI Cihamplas Walk',
  ];

  final List<DateTime> dates = List.generate(7, (index){
    DateTime now = DateTime.now();
    DateTime date = DateTime(now.year, now.month, now.day);
    return date.add(Duration(days: index));
  });

  final List<int> hours = List.generate(8, (index) => index + 12);

  String? selectedTheater;
  DateTime? selectedDate;
  int? selectedHour;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.all(24),
                child: BackNavigationBar(title: widget.movieDetail.title, onTap: () => ref.read(routerProvider).pop()),
              ),
              Padding(
                padding: const EdgeInsets.all(24),
                child: NetworkImageCard(
                  imageUrl: 'https://image.tmdb.org/t/p/w500/${widget.movieDetail.bakcdropPath ?? widget.movieDetail.posterPath}',
                  width: MediaQuery.of(context).size.width - 48,
                  height: (MediaQuery.of(context).size.width - 48) * 0.6,
                  borderRadius: 15,
                ),
              ),
              // theater option
              ...options(
                title: 'Select Theater',
                options: theaters,
                selectedItem: selectedTheater,
                onTap: (object) => setState(() {
                  selectedTheater = object;
                })
              ),
              verticalSpace(24),
              ...options(
                title: 'Select Date',
                options: dates,
                selectedItem: selectedDate,
                converter: (date) => DateFormat('EEE, d MMMM y').format(date),
                onTap: (object) => setState(() {
                  selectedDate = object;
                })
              ),
              verticalSpace(24),
              // time option
              ...options(
                title: 'Select Time',
                options: hours,
                selectedItem: selectedHour,
                converter: (hour) => '$hour:00',
                isOptionEnable: (hour) => selectedDate != null && DateTime(selectedDate!.year, selectedDate!.month, selectedDate!.day, hour).isAfter(DateTime.now()),
                onTap: (object) => setState(() {
                  selectedHour = object;
                })
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 24),
                child: SizedBox(
                  width: double.infinity,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      foregroundColor: backgroundColor,
                      backgroundColor: saffron,
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10))
                    ),
                    onPressed: () {
                      if(selectedTheater == null || selectedDate == null || selectedHour == null){
                        context.showSnackbar('Please select all options');
                      } else {
                        Transaction transaction = Transaction(
                          uid: ref.read(userDataProvider).value!.uid,
                          title: widget.movieDetail.title,
                          adminFee: 3000,
                          total: 0,
                          watchingTime: DateTime(selectedDate!.year, selectedDate!.month, selectedDate!.day, selectedHour!).millisecondsSinceEpoch,
                          transactionImage: widget.movieDetail.posterPath,
                          theaterName: selectedTheater!
                        );

                        ref.read(routerProvider).pushNamed('seat-booking', extra: (widget.movieDetail, transaction));
                      }
                    }, child: Text('Book Now'))
                ),
              )
            ],
          ),
        ],
      )
    );
  }
}