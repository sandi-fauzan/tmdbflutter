import 'package:flutter/material.dart';

class BottomNavBarItem extends StatelessWidget {
  final int index;
  final bool isSelected;
  final String title;
  final String image;
  final String selectedImage;

  const BottomNavBarItem({super.key, required this.index, required this.isSelected, required this.title, required this.image, required this.selectedImage});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: 24,
          height: 24,
          child: Image.asset(isSelected ? selectedImage : image),
        ),
        Text(
          title,
          style: const TextStyle(fontSize: 12, fontWeight: FontWeight.bold)
        )
      ],
    );
  }
}