import 'package:flutter/material.dart';
import 'package:learnriverpod/presentation/misc/constants.dart';

class SelectableCard extends StatelessWidget {
  final String text;
  final VoidCallback onTap;
  final bool isSelected;
  final bool isEnable;

  const SelectableCard({super.key, required this.text, required this.onTap, this.isSelected = false, this.isEnable = true});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: isEnable ? onTap : null,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: BoxDecoration(
          color: !isEnable ? Color.fromARGB(132, 113, 113, 113) : (isSelected ? saffron.withOpacity(0.4) : Colors.transparent),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(color: isEnable ? saffron : Colors.grey),
        ),
        child: Text(text),
      )
    );
  }
}