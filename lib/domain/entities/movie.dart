import 'package:freezed_annotation/freezed_annotation.dart';

part 'movie.freezed.dart';

@freezed
class Movie with _$Movie {
  const factory Movie({
    required int id,
    required String title,
    String? posterPath,
    // required String description,
    // required String director,
    // required String producer,
    // required String releaseDate,
    // required String duration,
    // required String genre,
    // required int rating,
    // required int price,
  }) = _Movie;

  factory Movie.fromJSON(Map<String, dynamic> json) => Movie(
    id: json['id'],
    title: json['title'],
    posterPath: json['poster_path'],
    // description: json['description'],
    // director: json['director'],
    // producer: json['producer'],
    // releaseDate: json['releaseDate'],
    // duration: json['duration'],
    // genre: json['genre'],
    // rating: json['rating'],
    // price: json['price'],
  );
}