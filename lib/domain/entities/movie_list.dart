import 'package:freezed_annotation/freezed_annotation.dart';

part 'movie_list.freezed.dart';

@freezed
class MovieList with _$MovieList {
  const factory MovieList({
    @required List<Movie> movies,
  }) = _MovieList;
}