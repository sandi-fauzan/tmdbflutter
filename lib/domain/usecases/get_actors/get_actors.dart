import 'package:learnriverpod/data/repositories/movie_repository.dart';
import 'package:learnriverpod/domain/entities/actor.dart';
import 'package:learnriverpod/domain/entities/result.dart';
import 'package:learnriverpod/domain/usecases/get_actors/get_actors_param.dart';
import 'package:learnriverpod/domain/usecases/usecase.dart';

class GetActors implements UseCase<Result<List<Actor>>, GetActorsParam> {
  final MovieRepository _movieRepository;

  GetActors({required MovieRepository movieRepository}) : _movieRepository = movieRepository;

  @override
  Future<Result<List<Actor>>> call(GetActorsParam params) async {
    var actorListResult = await _movieRepository.getActors(movieId: params.movieId, id: params.movieId);

    return switch (actorListResult) {
      Success(value: final actors) => Result.success(actors),
      Failed(message: final message) => Result.failed(message),
    };
  }
}