import 'package:learnriverpod/domain/entities/movie.dart';

class GetMovieDetailParam {
  final Movie movie;

  GetMovieDetailParam({required this.movie});
}